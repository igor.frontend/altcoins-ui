const { defineConfig } = require('@vue/cli-service')
const path = require("path");

module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: config => {
    config.plugin('define').tap(definitions => {
      definitions[0] = Object.assign(definitions[0], {
        'api.env': {
          API_ENDPOINT: '"' + process.env.API_ENDPOINT + '"',
          APP_ENV: '"' + process.env.APP_ENV + '"',
        },
      });
      return definitions;
    });
  },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, './src/assets/sass/app.scss')],
    },
  },
})
