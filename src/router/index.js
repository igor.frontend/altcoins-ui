import { createRouter, createWebHistory } from 'vue-router'
import Cookies from 'js-cookie';
import * as cookies from '../config/cookies'
import store from '../store';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [
      {
          path: '/',
          beforeEnter: (to, from, next) =>  {
              const token = Cookies.get(cookies.USER_TOKEN);
              if (token) {
                  const user = store.state.auth.user;
                  user
                      ? next()
                      : store.dispatch('setUserToken', token).then(() => next());
              } else {
                  next({name: 'login'})
              }
          }
      },
      {
        path: '/register',
        component: () => import('../pages/auth/RegisterPage.vue')
      },
      {
          path: '/login',
          component: () => import('../pages/auth/LoginPage.vue'),
          name: 'login'
      },
      {
          path: '/forgot',
          component: () => import('../pages/auth/ForgotPassword.vue')
      },
      {
          path: '/reset',
          component: () => import('../pages/auth/ResetPassword.vue')
      },
      {
          path: '/2fa',
          component: () => import('../pages/auth/TwoFactorAuth.vue'),
          name: '2fa'
      },
      {
          path: '/email-verification',
          component: () => import('../pages/auth/EmailVerification.vue'),
          name: 'email-verification'
      },
  ]
})

export default router
