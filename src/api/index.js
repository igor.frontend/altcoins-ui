import _ from 'lodash';
import axios from 'axios';
import router from '../router';

const REQUEST_ABORTED_ERROR_CODE = 'ECONNABORTED';
const DEFAULT_RESPONSE_ON_ABORTED_ERROR = { data: {} };

class Api {
    constructor() {
        this.axios = axios.create({
            baseURL: api.env.API_ENDPOINT + '/',
            headers: {
                Accept: 'application/json'
            }
        });

        this.axios.interceptors.response.use((response) => {
            return response;
        }, (error) => {
            if (_.has(error, 'response.data.message')) {
                if (error.response.data.message === 'Token has expired') {
                    router.push({name: 'login'});
                    return;
                }
            }
            if (error.code === REQUEST_ABORTED_ERROR_CODE) {
                return Promise.resolve(DEFAULT_RESPONSE_ON_ABORTED_ERROR);
            }
            return Promise.reject(error);
        });
    }

    setToken(token) {
        if (token) {
            this.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        } else {
            delete this.axios.defaults.headers.common['Authorization'];
        }
    }
}

export default new Api();
