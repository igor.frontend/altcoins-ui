import api from './';
import _ from "lodash";

class AuthApi {

    static login(credentials) {
        return api.axios.post('login', credentials)
            .then((response) => {
                return response.data.token;
            });
    }

    static register(data) {
        return api.axios.post('register', data)
    }

    static generateTwoFAQR(data) {
        return api.axios.post('generate-2fa-qr', data)
    }

    static me() {
        return api.axios.get('me').then((response) => {
            if (!_.isEmpty(response)) {
                return Object.assign({}, {user: response.data})
            }
        });
    }
}

export default AuthApi;