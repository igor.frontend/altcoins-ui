import * as types from './../mutation-types'

const state = {
    token: null,
    user: null,
};


const mutations = {
    [types.SET_USER_TOKEN](state, token) {
        state.token = token;
    },
    [types.SET_USER](state, user) {
        state.user = user;
    },
};

export default {
    state,
    mutations
}