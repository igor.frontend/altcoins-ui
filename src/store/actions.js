// import UserApi from '../api/user/user';
import AuthApi from '../api/auth';
import api from '../api';
import * as types from './mutation-types';
import * as cookies from '../config/cookies';
import Cookies from 'js-cookie';
import _ from 'lodash';

export const setUserByToken = _.throttle(
    ({ commit }) => {
        return AuthApi.me().then((data) => {
            data && commit(types.SET_USER, data)
        })
    } ,
    500,
    { trailing: false }
);

export const setUserToken = function({ commit, dispatch }, token) {
    api.setToken(token);
    Cookies.set(cookies.USER_TOKEN, token);
    commit(types.SET_USER_TOKEN, token);
    return dispatch('setUserByToken');
};

export const signIn = function({ dispatch }, credentials) {
    return AuthApi.login(credentials).then(token => {
        return dispatch('setUserToken', token);
    });
};