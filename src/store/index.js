import { createStore } from 'vuex'
import auth from "@/store/auth";
import * as actions from './actions'
export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions,
  modules: {
    auth,
  }
})
